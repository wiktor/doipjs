# doip.js

doip.js allows websites and Node.js projects to verify decentralized online
identities based on OpenPGP.

## Features

- Verify online identities using profile URLs
- Regex-based service provider detection
- [Mocha](https://mochajs.org/) tests

## About Keyoxide

[Keyoxide](https://keyoxide.org/), made by Yarmo Mackenbach, is a modern, secure
and privacy-friendly platform to establish decentralized online identities using
a novel concept know as [DOIP](doip.md). In an effort to make this technology
accessible for other projects and stimulate the emergence of both complementary
and competing projects, this project-agnostic library is
[published on codeberg.org](https://codeberg.org/keyoxide/doipjs) and open
sourced under the
[Apache-2.0](https://codeberg.org/keyoxide/doipjs/src/branch/main/LICENSE)
license.

## Community

There's a [Keyoxide Matrix room](https://matrix.to/#/#keyoxide:matrix.org) where
we discuss everything DOIP and Keyoxide.

## Donate

Please consider [donating](https://liberapay.com/Keyoxide/) if you think this
project is a step in the right direction for the internet.

## Funding

This library was realized with funding from
[NLnet](https://nlnet.nl/project/Keyoxide/).
