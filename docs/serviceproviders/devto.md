# dev.to

## Proof

Proofs are defined as posts and should contain the following information:

```
This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/FINGERPRINT) to [this dev.to account](https://dev.to/USERNAME). For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT` and `USERNAME`.

## Claim

```
https://dev.to/USERNAME/POST_TITLE
```

Make sure to replace `USERNAME` and `POST_TITLE`.
