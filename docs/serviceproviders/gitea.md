# gitea

## Proof

Proofs are defined by creating a public repository `gitea_proof` and setting the
description to:

```
[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://DOMAIN/USERNAME/gitea_proof
```

Make sure to replace `DOMAIN` and `USERNAME`.
