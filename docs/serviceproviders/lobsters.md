# lobste.rs

## Proof

Proofs are defined by adding the following information to the **About**:

```
This is an OpenPGP proof that connects my OpenPGP key to this Lobste.rs account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://lobste.rs/u/USERNAME
```

Make sure to replace `USERNAME`.
