# mastodon

## Proof

Proofs are defined by adding an item under `Profile metadata` with `OpenPGP` as
label and the fingerprint as value.

## Claim

```
https://DOMAIN/@USERNAME
```

Make sure to replace `DOMAIN` and `USERNAME`.
