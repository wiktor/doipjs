# twitter

## Proof

Proofs are defined as tweets and should contain the following information:

```
This is an OpenPGP proof that connects my OpenPGP key to this Twitter account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://twitter.com/USERNAME/status/TWEET_ID
```

Make sure to replace `USERNAME` and `TWEET_ID`.
