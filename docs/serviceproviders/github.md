# github

## Proof

Proofs are defined by creating a public gist and a file `openpgp.md` with the
following information:

```
This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/FINGERPRINT) to [this Github account](https://github.com/USERNAME). For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT` and `USERNAME`.

## Claim

```
https://gist.github.com/USERNAME/GIST_ID
```

Make sure to replace `USERNAME` and `GIST_ID`.
