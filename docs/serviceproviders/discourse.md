# discourse

## Proof

Proofs are defined by including the following information in the **About me**:

```
This is an OpenPGP proof that connects my OpenPGP key to this Discourse account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://DOMAIN/u/USERNAME
```

Make sure to replace `DOMAIN` and `USERNAME`.
