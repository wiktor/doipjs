# hackernews

## Proof

Proofs are defined by adding the following information to the **about**:

```
This is an OpenPGP proof that connects my OpenPGP key to this Hackernews account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://news.ycombinator.com/user?id=USERNAME
```

Make sure to replace `USERNAME`.
