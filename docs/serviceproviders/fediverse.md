# fediverse

Fediverse refers to the network of ActivityPub-compatible servers.

## Proof

Proofs are defined by adding the following information to the **Bio** or
similar field:

```
[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://DOMAIN/users/USERNAME
```

Make sure to replace `DOMAIN` and `USERNAME`.
