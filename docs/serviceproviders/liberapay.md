# liberapay

## Proof

Proofs are defined by adding the following information to a **Statement**:

```
[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://liberapay.com/USERNAME
```

Make sure to replace `USERNAME`.
