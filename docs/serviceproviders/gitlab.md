# gitlab

## Proof

Proofs are defined by creating a public project with slug `gitea_proof` and
setting the description to:

```
[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://DOMAIN/USERNAME/gitlab_proof
```

Make sure to replace `DOMAIN` and `USERNAME`.
