# dns

## Proof

Proofs are defined by adding a **TXT** record to the DNS records:

```
openpgp4fpr:FINGERPRINT
```

Make sure to replace `FINGERPRINT`.

## Claim

```
dns:DOMAIN
```

Make sure to replace `DOMAIN`.
