# reddit

## Proof

Proofs are defined as posts and should contain the following information:

```
This is an OpenPGP proof that connects my OpenPGP key to this Reddit account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:FINGERPRINT]
```

Make sure to replace `FINGERPRINT`.

## Claim

```
https://reddit.com/user/USERNAME/comments/POST_ID/POST_TITLE
```

Make sure to replace `USERNAME`, `POST_ID` and `POST_TITLE`.
