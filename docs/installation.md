# Installation

Install using **yarn**:

```bash
yarn add doipjs
```

Install using **NPM**:

```bash
npm install --save doipjs
```

Install on website by including the following HTML snippet:

```html
<script src="https://cdn.jsdelivr.net/npm/doipjs@0.7.5/dist/doip.min.js"></script>
```

Next step: [quick start (Node.js)](quickstart-nodejs.md) and [quick start (browser)](quickstart-browser.md)
