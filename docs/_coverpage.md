# doip.js <small>0.7.5</small>

<img src="doip.png" width="120">

Decentralized OpenPGP Identity Proofs library in Node.js

[Source code](https://codeberg.org/keyoxide/doipjs)
[Getting started](#doipjs)

![color](#c9beff)
