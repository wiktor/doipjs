# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.7.5] - 2020-12-08
### Fixed
- Browser bundling

## [0.7.4] - 2020-12-08
### Fixed
- Handling HKP URI

## [0.7.3] - 2020-12-08
### Fixed
- Bundled library for release

## [0.7.2] - 2020-12-08
### Fixed
- Support for specifying keyservers

## [0.7.1] - 2020-12-08
### Changed
- Update openpgpjs dependency

## [0.7.0] - 2020-12-05
### Changed
- Properly reject promises

## [0.6.0] - 2020-11-20
### Changed
- Handle multiple users in key

## [0.5.2] - 2020-11-20
### Fixed
- Claim verification regex

## [0.5.1] - 2020-11-20
### Fixed
- Link to bundled openpgp

## [0.5.0] - 2020-11-18
### Added
- Keys fetching using openpgp.js
- Claims extraction from keys using openpgp.js

### Changed
- Support xmpp via doip-proxy
- Module structure
- Docs

### Fixed
- Bad verification value return for text proofs
- Missing User-Agent request header

## [0.4.2] - 2020-11-06
### Changed
- URLs in package.json

## [0.4.1] - 2020-11-06
### Changed
- Update README
- Add image to coverpage

## [0.4.0] - 2020-11-06
### Added
- Custom request handler for DNS service provider
- Docs

### Changed
- Service provider data structure
- More consistent handling of options

### Removed
- dotenv dependency

### Fixed
- Crash for unexpected JSON data structure
- Body in http requests

## [0.3.0] - 2020-11-04
### Added
- Liberapay service provider
- Proxy request handler

### Changed
- Improve handling of arrays in JSON
- Customizable proxy hostname

### Fixed
- Dots in URL regex

## [0.2.0] - 2020-11-03
Initial release
